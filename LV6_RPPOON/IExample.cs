﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV6_RPPOON
{
    interface IExample
    {
        System.String Name { get; }
        void Run();
    }
}
