﻿using LV6_RPPOON.Zadatak_1;
using LV6_RPPOON.Zadatak_2;
using LV6_RPPOON.Zadatak_3;
using LV6_RPPOON.Zadatak_4;
using LV6_RPPOON.Zadatak_5;
using LV6_RPPOON.Zadatak_6_7;
using System;
using System.Collections.Generic;

namespace LV6_RPPOON
{
    class Program
    {
        static void Main(string[] args)
        {
            List<IExample> examples = new List<IExample>()
            {
                new Example_1(),
                new Example_2(),
                new Example_3(),
                new Example_4(),
                new Example_5(),
                new Example_6(),
                new Example_7()
            };

            foreach (IExample example in examples)
            {
                PrintUtilities.PrintStart(example.Name);
                example.Run();
                PrintUtilities.PrintEnd();
            }

        }
    }
}
