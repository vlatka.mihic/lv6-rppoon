﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV6_RPPOON.Zadatak_2
{
    interface IAbstractProductCollection
    {
        IAbstractProductIterator GetIterator();
    }
}
