﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV6_RPPOON.Zadatak_2
{
    class Example_2: IExample
    {
        public string Name
        {
            get
            {
                return this.ToString();
            }
        }
        public void Run()
        {
            Product milk = new Product("2% Reduced Fat Milk", 5.99);
            Product bread = new Product("White Bread", 7.89);
            Product eggs = new Product("Medium Eggs", 12.99);

            Box breakfast = new Box();
            breakfast.AddProduct(milk);
            breakfast.AddProduct(bread);
            breakfast.AddProduct(eggs);

            IAbstractProductIterator iterator = breakfast.GetIterator();
            for (Product product = iterator.First(); iterator.IsDone == false; product = iterator.Next())
            {
                Console.WriteLine(product);
            }
        }
    }
}
