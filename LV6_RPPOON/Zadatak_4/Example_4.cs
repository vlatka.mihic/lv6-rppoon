﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV6_RPPOON.Zadatak_4
{
    class Example_4: IExample
    {
        public string Name
        {
            get
            {
                return this.ToString();
            }
        }
        public void Run()
        {
            BankAccount bankAccount = new BankAccount("Vlatka Mihic", "Andrije Hebranga 61, Osijek", 2154);
            Console.WriteLine(bankAccount);
            Console.WriteLine("\nOutta here...");
            bankAccount.ChangeOwnerAddress("Stitarska 32, Zupanja");
            Console.WriteLine(bankAccount);
            AccountState accountState = bankAccount.StoreState();

            Console.WriteLine("\nSalary...");
            bankAccount.UpdateBalance(1534);
            Console.WriteLine(bankAccount);

            Console.WriteLine("\nShopping time!!");
            bankAccount.RestoreState(accountState);
            Console.WriteLine(bankAccount);
        }
    }
}
