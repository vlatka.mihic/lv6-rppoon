﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV6_RPPOON.Zadatak_1
{
    class Example_1: IExample
    {
        public string Name
        {
            get
            {
                return this.ToString();
            }
        }
        public void Run()
        {
            Note englishClass = new Note("English class","14.05.2020 - 13:00h");
            Note informationTheory = new Note("Homework 1", "15.05.2020 - 24:00h");
            Note rppoon = new Note("Laboratory exercises 6", "14.05.2020 - 24:00h");

            Notebook reminder = new Notebook();
            reminder.AddNote(englishClass);
            reminder.AddNote(informationTheory);
            reminder.AddNote(rppoon);

            IAbstractIterator iterator = reminder.GetIterator();
            for (Note note = iterator.First(); iterator.IsDone == false; note = iterator.Next())
            {
                note.Show();
            }
        }
    }
}
