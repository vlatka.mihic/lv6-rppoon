﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV6_RPPOON.Zadatak_1
{
    interface IAbstractCollection
    {
        IAbstractIterator GetIterator();
    }
}
