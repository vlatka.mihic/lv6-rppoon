﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV6_RPPOON.Zadatak_6_7
{
    class Example_6: IExample
    {
        public string Name
        {
            get
            {
                return this.ToString();
            }
        }
        public void Run()
        {
            Console.WriteLine("Enter password to check: ");
            String password = Console.ReadLine();
            StringChecker digitChecker = new StringDigitChecker();
            StringChecker lenghtChecker = new StringLengthChecker();
            StringChecker upperCaseChecker = new StringUpperCaseChecker();
            StringChecker lowerCaseChecker = new StringLowerCaseChecker();

            digitChecker.SetNext(lenghtChecker);
            lenghtChecker.SetNext(upperCaseChecker);
            upperCaseChecker.SetNext(lowerCaseChecker);

            if (digitChecker.Check(password))
            {
                Console.WriteLine("Valid password!");
            }
            else
            {
                Console.WriteLine("Invalid password!!");
            }

        }
    }
}
