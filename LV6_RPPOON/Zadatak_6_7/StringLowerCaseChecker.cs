﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LV6_RPPOON.Zadatak_6_7
{
    class StringLowerCaseChecker : StringChecker
    {
        protected override bool PerformCheck(string stringToCheck)
        {
            return stringToCheck.Any(ch => char.IsLower(ch));
        }
    }
}
