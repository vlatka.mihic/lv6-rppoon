﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV6_RPPOON.Zadatak_6_7
{
    class Example_7: IExample
    {
        public string Name
        {
            get
            {
                return this.ToString();
            }
        }
        public void Run()
        {
            Console.WriteLine("Enter password to check: ");
            String password = Console.ReadLine();
            StringChecker digitChecker = new StringDigitChecker();
            StringChecker lenghtChecker = new StringLengthChecker();
            StringChecker upperCaseChecker = new StringUpperCaseChecker();
            StringChecker lowerCaseChecker = new StringLowerCaseChecker();

            PasswordValidator passwordValidator = new PasswordValidator(digitChecker);
            passwordValidator.AddNext(lenghtChecker);
            passwordValidator.AddNext(upperCaseChecker);
            passwordValidator.AddNext(lowerCaseChecker);

            passwordValidator.Validator(password);

        }
    }
}
