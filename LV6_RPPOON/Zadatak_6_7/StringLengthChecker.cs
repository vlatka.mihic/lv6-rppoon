﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV6_RPPOON.Zadatak_6_7
{
    class StringLengthChecker : StringChecker
    {
        private int stringLength = 8;
        protected override bool PerformCheck(string stringToCheck)
        {
            return stringToCheck.Length <= stringLength;
        }
    }
}
