﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV6_RPPOON.Zadatak_6_7
{
    class PasswordValidator
    {
        StringChecker firstChecker;
        StringChecker lastChecker;
        public PasswordValidator(StringChecker stringChecker)
        {
            this.firstChecker = stringChecker;
            this.lastChecker = stringChecker;
            this.firstChecker.SetNext(this.lastChecker);
        }
        public void AddNext(StringChecker stringChecker)
        {
            this.lastChecker.SetNext(stringChecker);
            this.lastChecker = stringChecker;
        }
        public void Validator(String password)
        {
            if (firstChecker.Check(password))
            {
                Console.WriteLine("Valid password!");
            }
            else
            {
                Console.WriteLine("Invalid password!!");
            }
        }
        
    }
}
