﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV6_RPPOON.Zadatak_3
{
    class Example_3:IExample
    {
        public string Name
        {
            get
            {
                return this.ToString();
            }
        }
        public void Run()
        {
            CareTaker careTaker = new CareTaker();
            ToDoItem toDoItem = new ToDoItem("RPPOON-LV6","Predaja izvještaja", new DateTime(2020, 05, 14, 20, 00, 00));

            Console.WriteLine(toDoItem);
            toDoItem.ChangeTask("Learn C#");
            Console.WriteLine(toDoItem);
            careTaker.StoreMemento(toDoItem.StoreState());

            toDoItem.ChangeTimeDue(new DateTime(2020, 05, 20, 16, 34, 07));
            Console.WriteLine(toDoItem);

            Console.WriteLine("Restoring last state...");
            toDoItem.RestoreState(careTaker.GetLastMemento());
            Console.WriteLine(toDoItem);


        }
    }
}
