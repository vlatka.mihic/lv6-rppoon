﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LV6_RPPOON.Zadatak_3
{
    class CareTaker
    {
        List<Memento> mementos = new List<Memento>();
        public void StoreMemento(Memento memento) { this.mementos.Add(memento); }
        public void RemoveMemento(Memento memento) 
        {
            if (memento != null)
            {
                this.mementos.Remove(memento);
            }
        }
        public Memento GetLastMemento() { return mementos.OrderByDescending(s => s.CreationTime).First(); }
    }
}
