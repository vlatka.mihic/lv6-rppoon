﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV6_RPPOON.Zadatak_5
{
    class Example_5: IExample
    {
        public string Name
        {
            get
            {
                return this.ToString();
            }
        }
        public void Run()
        {
            AbstractLogger logger = new ConsoleLogger(MessageType.ALL);
            FileLogger fileLogger =
            new FileLogger(MessageType.ERROR | MessageType.WARNING, "logFile.txt");
            
            logger.SetNextLogger(fileLogger);
            logger.Log("First message", MessageType.ERROR);
            logger.Log("Second message", MessageType.WARNING);
            logger.Log("Third message", MessageType.INFO);
        }
    }
}
